package ru.nlmk.nikulin.tm;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import ru.nlmk.nikulin.tm.Services.MyService;

import static org.junit.jupiter.api.Assertions.*;


class СalculationsTest {
    private Сalculations сalculations;
    MyService myService = new MyService();

    @BeforeEach
    void setUp() {

        сalculations = new Сalculations();
    }


    @Test
    void sumResultOk() {

        assertEquals(6, сalculations.sum("3", "3"));
        assertEquals(-5, сalculations.sum("-7", "2"));
    }

    @Test
    void sumResultNotOk() {

        assertThrows(IllegalArgumentException.class, () ->  сalculations.sum("7.2", "4"));
        assertThrows(IllegalArgumentException.class, () ->  сalculations.sum("2", "0.5"));
        assertThrows(IllegalArgumentException.class, () ->  сalculations.sum("", "0.1"));
        assertThrows(IllegalArgumentException.class, () ->  сalculations.sum("10", ""));
    }

    @Test
    void numFibonacciIsOk() {
        long[] sequence = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
        assertArrayEquals(sequence, сalculations.numFibonacci("15"));
    }

    @Test
    void numFibonacciIsNotOk() {
        assertThrows(IllegalArgumentException.class, () -> сalculations.numFibonacci("20"));
        assertThrows(IllegalArgumentException.class, () -> сalculations.numFibonacci("-16"));
        assertThrows(IllegalArgumentException.class, () -> сalculations.numFibonacci("text"));
    }

    @Test
    void factorialIsOk() {
         assertEquals(2, сalculations. factorial("0"));
         assertEquals(15, сalculations.factorial("4"));
        }
    @Test
    void factorialIsNotOk() {
            assertThrows(IllegalArgumentException.class, () -> сalculations.factorial("-3"));
            assertThrows(IllegalArgumentException.class, () -> сalculations.factorial("text"));
        }

}