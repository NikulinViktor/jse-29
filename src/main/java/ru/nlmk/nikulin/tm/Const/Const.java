package ru.nlmk.nikulin.tm.Const;

public class Const {
    public static final String STOP = "exit";
    public static final String SUM = "sum";
    public static final String HELP = "help";
    public static final String FACTORIAL = "factorial";
    public static final String FIBONACHI = "fib";
    public static final String OTHER = "comand not found";
    public static final int    OK = 0;
    public static final int    ERROR = -1;

}
