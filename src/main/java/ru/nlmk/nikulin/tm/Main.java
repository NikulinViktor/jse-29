package ru.nlmk.nikulin.tm;

import ru.nlmk.nikulin.tm.Services.MyService;
import static ru.nlmk.nikulin.tm.Const.Const.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("-___Mathematics operations programm___-");
        System.out.println("<-Enter what do you want calculate->");
        String str = "";
        while (!STOP.equals(str)) {
            str = scanner.nextLine();
            run(str);
        }

    }

    private static long run (final String val_str){
        Scanner scanner = new Scanner(System.in);
        Сalculations сalculations = new Сalculations();
        MyService myService = new MyService();
        if (val_str == null || val_str.isEmpty()) return -1;
        try {
            switch (val_str) {
                case HELP: {
                    return helpInfo();
                }
                case SUM: {
                    System.out.println("Enter first argument for Sum operation ->");
                    String arg1 = scanner.nextLine();
                    System.out.println("Enter second argument for Sum operation ->");
                    String arg2 = scanner.nextLine();
                    System.out.println(сalculations.sum(arg1, arg2));
                    return OK;
                }
                case FACTORIAL: {
                    System.out.println("Enter value ->");
                    String arg = scanner.nextLine();
                    System.out.println(сalculations.factorial(arg));
                    return OK;
                }
                case FIBONACHI: {
                    System.out.println("Enter size fibonachi ->");
                    String arg = scanner.nextLine();
                    if(myService.toLong(arg) < 0L || arg == null){
                        throw new IllegalArgumentException("Please, enter positive number");
                    }
                    System.out.println(сalculations.numFibonacci(arg));
                    return OK;
                }
            }
    }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return ERROR;
    }
    private static int helpInfo() {
        System.out.println("help      - Доступные команды");
        System.out.println("sum       - Сумма чисел");
        System.out.println("factorial - Факториал");
        System.out.println("fib       - Число Фибоначчи");
        System.out.println("exit      - Завершить сеанс.");
        return OK;
    }

}
