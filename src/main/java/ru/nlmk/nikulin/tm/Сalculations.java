package ru.nlmk.nikulin.tm;

import ru.nlmk.nikulin.tm.Services.MyService;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class Сalculations {

    public long sum(String arg1, String arg2) {
        MyService myService = new MyService();

        return myService.toLong(arg1) + myService.toLong(arg2);
    }


    public long[] numFibonacci(String arg) {
        MyService myService = new MyService();
        long valLong = myService.iSLessNull(myService.toLong(arg));
        List<Long> list = new LinkedList<>();
        list.add(1L);
        list.add(1L);

        long num0 = 1L;
        long num1 = 1L;
        long sumNearnumbers = 1L;
        for (long i = 0; i < valLong; i++) {
            sumNearnumbers = num0 + num1;
            num0 = num1;
            num1 = sumNearnumbers;
            list.add(sumNearnumbers);
            if (valLong == sumNearnumbers) {
                break;
            }
            if (sumNearnumbers >= valLong + 10000L) {
                throw new IllegalArgumentException("Attention! The number do not be decomposed into a fibonacci series becouse a value to match.");
            }
        }
        long[] result = new long[list.size()];
        String[] res = new String[list.size()];
        int n;
        for (n = 0; n < list.size(); n++) {
            result[n] = list.get(n);
            res[n] = String.valueOf(list.get(n));

        }
        System.out.println(Arrays.toString(res));
        return result;
    }


    public long factorial(String arg) {
        MyService myService = new MyService();
        long n = myService.toLong(arg);
        long result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;

    }

}
