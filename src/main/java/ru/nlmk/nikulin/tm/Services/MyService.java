package ru.nlmk.nikulin.tm.Services;

public class MyService {
    public long toLong(String arg){
        try {
            return Long.parseLong(arg);
        } catch (NumberFormatException | NullPointerException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }
    public long iSLessNull (long arg){
        if (arg <0) {
            System.out.println("Отрицательное значение аргумента");
            throw new IllegalArgumentException();
        }
        return arg;
    }

}
